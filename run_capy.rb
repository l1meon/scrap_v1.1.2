require 'rubygems'
require 'capybara'
require 'capybara/webkit'
require 'capybara/dsl'
require 'headless'
require_relative 'lib/config/environment'
require_relative 'lib/models/proxy'
require_relative 'lib/models/used_proxy'
require_relative 'lib/models/video_statistic'
require_relative 'lib/script/browser/driver_reg'
require_relative 'lib/script/browser/user_agents'
require_relative 'lib/script/algorithm/per_day'

#VIDEO_1 = 'https://www.youtube.com/watch?v=SMqEhhVjQ74'
#VIDEO_2 = 'https://www.youtube.com/watch?v=uuJLnqpL88U'

module Hack
  class Version1
    include Capybara::DSL
    include Browser::DriverReg
    include Browser::UserAgents
    include Algorithm::PerDay

    # create Algorith sleep-list
    Algorithm::PerDay.create_list

    # begin algorith loop
    def start(url)
      @headless_dimensions = [
          '2560x1440x24',
          '1920x1080x24',
          '1600x1200x24',
          '1600x900x24',
          '1024x768x24'
      ]
      while Algorithm::PerDay.show_list.size > 1
        begin
          Algorithm::PerDay.show_list.each do |hour, time_values|
            time_values.each do |time|
              values_count = time_values.size
              # start first hour
              (1..values_count).each do
                #@headless_dimensions.shuffle!
                Proxy.all.each do |proxy|
                  #@headless = Headless.new(dimensions: @headless_dimensions.sample)
                  #@headless.start
                  puts "Starting with ip: #{proxy.ip} on port: #{proxy.port}"
                  Browser::DriverReg.driver_config(ip: proxy.ip, port: proxy.port, user_agent: Browser::UserAgents.agents.sample)
                  visit(url)
                  # sleep
                  sleep(rand(90..180)) # Watching video
                  UsedProxy.create!(proxy.as_json)
                  VideoStatistics.create!(
                      video_url: url,
                      before_views: nil,
                      after_views: nil,
                      ip: proxy.ip,
                      port: proxy.port,
                      headers: response_headers
                  )
                  # sleep for next video to watch
                  sleep(time) # sleep time from hour sleep list
                  time_values.delete_at(0)
                  Algorithm::PerDay.show_list.delete(hour)
                  #@headless.destroy
                end
              end
            end
          end
          if Algorithm::PerDay.show_list.size <= 2
            Algorithm::PerDay.create_list
          end
        rescue Exception => error
          puts error
          next
        ensure
          #@headless.destroy if @headless
        end
      end
    end

    # def get_site(url)
    #   visit(url)
    #   # sleep
    #   sleep(rand(90..180)) # Watching video
    # end

  end
end

hack = Hack::Version1.new
hack.start('https://www.youtube.com/watch?v=uuJLnqpL88U')
# module MyConfig
#
#   def self.driver_config
#     Capybara.register_driver :webkit do |app|
#       user_agent = { agent: 'HTTP_USER_AGENT'}
#       Capybara::Webkit::Driver.new(app,
#         :headers => {user_agent[:agent] => 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:43.0) Gecko/20100101 Firefox/43.0'}
#         #:proxy => { host:'217.106.65.253', port: 3128 }
#       )
#     end
#     Capybara.run_server = false
#     Capybara.current_driver = :webkit
#   end
# end
#
# module Hack
#   class Test
#     include Capybara::DSL
#     include MyConfig
#
#     MyConfig.driver_config
#
#     def get_site
#       visit('http://ifconfig.co')
#       puts response_headers
#       ip = find('.ip')
#       puts ip.text
#     end
#   end
# end
#
# hack = Hack::Test.new
# hack.get_site