require_relative 'lib/config/environment'

namespace :db do

  desc "Migrate the database"
  task :migrate do
    ActiveRecord::Migrator.migrate('lib/db/migrate')
    puts "Database Migrated"
  end

  desc 'Rollback migration'
  task :rollback do
    ActiveRecord::Migrator.rollback('lib/db/migrate')
    puts "Rollback to version: #{ENV['VERSION'].to_i}"
  end
end

namespace :g do
  desc "Generate migration"
  task :migration do
    name = ARGV[1] || raise("Specify name: rake g:migration your_migration")
    timestamp = Time.now.strftime("%Y%m%d%H%M%S")
    path = File.expand_path("../lib/db/migrate/#{timestamp}_#{name}.rb", __FILE__)
    migration_class = name.split("_").map(&:capitalize).join

    File.open(path, 'w') do |f|
      f.write <<-EOF
class #{migration_class} < ActiveRecord::Migration
  def self.up
  end

  def self.down
  end
end
      EOF
    end
    puts "Migration #{path} created"
    abort # need to stop other tasks
  end
end