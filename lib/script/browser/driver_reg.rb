module Browser
  module DriverReg
    def self.driver_config(ip: nil, port: nil, user_agent: nil)
      Capybara.register_driver :webkit do |app|
        ua = { agent: 'HTTP_USER_AGENT'}
        Capybara::Webkit::Driver.new(app,
                                     :headers => {ua[:agent] => user_agent.to_s}
        #, proxy: { ip: ip.to_s, port: port.to_i}
        )
      end
      Capybara.run_server = false
      Capybara.current_driver = :webkit
    end
  end
end