module Browser
  module SearchMethod
    class << self

      def search_type(method, opts={})
        case method
          when 'youtube_query'
            youtube_query(title: opts[:title])
          when 'google_query'
            google_query
          when 'youtube_channel'
            youtube_channel(channel: opts[:channel], title: opts[:title])
          else
            open_url(opts[:video_url])
        end
      end

      def youtube_query(title: nil)
        title = title
        yt_url = 'http://youtube.com'
        search_class = '.search_query'
        visit(yt_url)
      end

      def google_query
        g_url = 'http://google.com'
        visit(g_url)
      end

      def youtube_channel(channel: nil, title: nil)
        yt_url = 'http://youtube.com'
        channel = channel
        title = title
        search_class = '.search_query'

      end

      def open_url(video_url)
        video_url = 'video_url'
        visit(video_url)
        random_sleep_time = Random.new.rand(180..300)
        sleep(random_sleep_time)
      end

    end
  end
end