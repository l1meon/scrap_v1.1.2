module Algorithm
  module TestingVersion1
    class << self
      def starting_points
        @hour_array ||= (1..60).to_a
        @used_from_hour ||= []
      end

      def shuffle_times(array,number)
        number.times do
          array.shuffle!
        end
        array
      end

      def start
        @using_nr = @hour_array.sample
        puts "Picked: #{@using_nr}"
        @used_from_hour << @using_nr
        @hour_array.delete(@using_nr)
        shuffle_times(@hour_array, @hour_array.count)
        #print @hour_array
        puts @hour_array.count
      end

      def init_hours
        @hours ||= {}
      end

      def randomizer
        counter = 0
        24.times do |i|
          random_count = rand(3..11)
          sym_var = "number_#{i}"
          init_hours[sym_var] = Array.new(random_count){ rand(3..23) }.uniq!
          while init_hours[sym_var].nil?
            init_hours[sym_var] = Array.new(random_count){ rand(3..23) }.uniq!
          end
          counter += random_count
          puts "per Hour: #{random_count}"
          #puts "per day: #{counter}"
        end
      end
    end
  end
end

Algorithm::TestingVersion1.randomizer
puts Algorithm::TestingVersion1.init_hours
# while Algorithm::TestingVersion1.starting_points.count <= 60
#   Algorithm::TestingVersion1.start
# end