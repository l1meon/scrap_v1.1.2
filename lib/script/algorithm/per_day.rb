module Algorithm
  module PerDay
    class << self
      $per_day_list ||= {}

      def create_list
        @list ||= {}
        24.times do |i|
          @name = "hour_#{i}".to_sym
          begin
            begin
              @hour_array = Array.new(rand(3..17)){rand(3..23)}.uniq!
            end until !@hour_array.nil?
          end until @hour_array.inject(&:+) <= 57
          @hour_array.delete(nil) if @hour_array.include?(nil)
          @list[@name] = @hour_array
          $per_day_list[@name] = @hour_array
        end
        @list
      end

      def show_list
        $per_day_list
      end
    end
  end
end