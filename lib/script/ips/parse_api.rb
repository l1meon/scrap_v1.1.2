require 'rubygems'
require 'net/http'
require 'json'
require_relative '../../config/environment'
require_relative '../../models/proxy'
module Ips
  module ParseApi
    class << self
      HIDE_ME_API_KEY = '411756811663508'
      HIDE_ME_API_URL = 'http://hideme.ru/api/proxylist.php?'

      def from_url
        @all_ips ||= []
        url = "#{HIDE_ME_API_URL}out=js&maxtime=1000&type=hs45&anon=3,4&uptime=95&code=#{HIDE_ME_API_KEY}"
        uri = URI(url)
        response = Net::HTTP.get(uri)
        @all_ips = JSON.parse(response)

        @all_ips.each do |hash|
          @p_type = ''
          if hash['http'] == '1'
            @p_type = 'http'
          elsif hash['https'] == '1'
            @p_type = 'https'
          elsif hash['socks4'] == '1'
            @p_type = 'socks4'
          elsif hash['socks5'] == '1'
            @p_type = 'socks5'
          else
            @p_type = 'Error'
          end

          @level_ = ''
          if hash['anon'] == '3'
            @level_ = 'Anonim'
          elsif hash['anon'] == '4'
            @level_ = 'Elite'
          else
            @level_ = 'Error'
          end
          begin
            save_to_db(
                ip: hash['ip'],
                port: hash['port'],
                country: hash['country'],
                p_type: @p_type,
                level_: @level_,
                speed: nil,
                maxtime: hash['maxtime'],
                uptime: hash['uptime']
            )
          rescue Exception => e
            puts e
            next
          end
        end
      end

      def save_to_db(opts={})

        Proxy.create!(
                 ip: opts[:ip],
                 port: opts[:port],
                 country: opts[:country],
                 proxy_type: opts[:p_type],
                 level: opts[:level_],
                 speed: nil,
                 max_response_time: opts[:maxtime],
                 uptime: opts[:uptime]
        )
      end

      def count_ips
        Proxy.all.count
      end
    end
  end
end

puts Ips::ParseApi.count_ips

Ips::ParseApi.from_url

puts Ips::ParseApi.count_ips
