require_relative '../../config/environment'
require_relative '../../models/proxy'
require_relative '../../models/new_proxy'
require 'csv'
module Ips
  module Add
    class << self

      def from_txt_file(full_file_path)
        File.open(full_file_path) do |file|
          file.each do |line|
            port = line.strip.scan(/\d*\z/).join('')
            ip = line.strip
            ip.slice!(':')
            ip.slice!(port)
            puts "Ip: #{ip} : #{port} -- Adding to database"
            begin
              #Proxy.create!(ip: ip.to_s, port: port.to_i)
              NewProxy.create!(ip: ip.to_s, port: port.to_i)
            rescue Exception => e
              puts e
              next
            end
          end
        end
      end

      def from_csv_file(full_file_path)
        @arr ||= []
        file = File.open(full_file_path, "r:ISO-8859-1")
        csv = CSV.parse(file.read.encode('UTF-8'), col_sep: ';', headers: true)
        csv.each do |row|
          h = row.to_h
          @arr << h
        end
        # puts @arr
        # @arr.each do |hash|
        #   anonym = 'Высокая'
        #   anonym_m = 'Средняя'
        #   if hash['anonym'] == anonym || hash['anonym'] == anonym_m
        #     NewProxy.create!(hash.except('country_code','country_name','city','time_http','time_ssl','time_socks4','time_socks5','result_http','result_ssl','result_socks4','result_cosk5'))
        #   end
        #
        #end
        @arr.each do |hash|
          resp_time = 1100
          begin
            if hash['time_http'].to_i <= resp_time || hash['time_ssl'].to_i <= resp_time
              ip = hash['host']
              port = hash['port']
              puts "Add ip from CSV FILE"
              NewProxy.create!(ip: ip.to_s, port: port.to_i)
            end
          rescue Exception => except
            puts except
            next
          end
        end
      end

      def check_and_save

      end
    end
  end
end
#puts "Add from text ***********************"
#Ips::Add.from_txt_file('/home/milana/hack/capy_bara/ips/c1t.txt')
puts "Add from fox tools ***********++++++++++++++++"
Ips::Add.from_csv_file('/home/milana/hack/capy_bara/ips/fox_tools.csv')
puts "Add from c2 ++++++++++++++++++++++++++++-------------------"
Ips::Add.from_csv_file('/home/milana/hack/capy_bara/ips/c2.csv')
puts "Add from c3 --------------------------------------------"
Ips::Add.from_csv_file('/home/milana/hack/capy_bara/ips/c3.csv')
puts "Add from c4 ########################################"
Ips::Add.from_csv_file('/home/milana/hack/capy_bara/ips/c4.csv')