class CreateUsedProxiesTable < ActiveRecord::Migration
  def self.up
    create_table :used_proxies do |t|
      t.string :ip # 127.0.0.1
      t.integer :port # 8080
      t.string :country # RU / MD / UK /
      t.string :proxy_type # HTTP(S) / SOCKS4(5)
      t.string :level # Elite, Anonym
      t.string :speed # 1(best) / 2(average) / 3(slow)
      t.string :max_response_time # Time in ms
      t.string :uptime # Proxy uptime (1 - 100)
      t.time :last_used, default: Time.now
      t.integer :used_times
    end
  end

  def self.down
    drop_table :used_proxies
  end
end
