class CreateVideoStatisticsTable < ActiveRecord::Migration
  def self.up
    create_table :video_statistics do |t|
      t.string :video_url
      t.integer :before_views
      t.integer :after_views
      t.time  :at_time, default: Time.now
      t.string :ip
      t.string :port
      t.text :headers
    end
  end

  def self.down
    drop_table :video_statistics
  end
end
